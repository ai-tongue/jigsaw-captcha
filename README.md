# jigsaw-captcha

#### 介绍
Java实现的滑动拼图验证码后端

#### 操作截图
详情请查看前端示例：[vue-jigsaw-captcha](https://gitee.com/ai-tongue/vue-jigsaw-captcha)  
* 默认状态  
![open](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/open.png)
* 滑动中  
![slice](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/slice.png)
* 操作成功  
![success](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/success.png)
* 操作失败  
![error](https://gitee.com/ai-tongue/vue-jigsaw-captcha/raw/master/static/screenshot/error.png)

#### 关连项目
后端示例：[jigsaw-captcha-demo](https://gitee.com/ai-tongue/jigsaw-captcha-demo)  
前端示例：[vue-jigsaw-captcha](https://gitee.com/ai-tongue/vue-jigsaw-captcha)

#### 安装教程
``` bash
# install dependencies
mvn install
```

#### 使用说明
```xml
<dependency>
    <groupId>com.mxjsxz</groupId>
    <artifactId>jigsaw-captcha</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
