package com.mxjsxz.jigsawcaptcha.model.form;

/**
 * 拼图验证码检查
 *
 * @author xdl
 * @since 2019-9-5
 */
public class CaptchaCheckForm {
    
    /**
     * 登录名
     */
    private String username;
    
    /**
     * 登录密码
     */
    private String password;
    
    /**
     * 注册token
     */
    private String token;
    
    /**
     * 滑动x坐标
     */
    private Integer sliceX;
    
    public CaptchaCheckForm() {
    }
    
    public CaptchaCheckForm(String username, String password, String token, Integer sliceX) {
        this.username = username;
        this.password = password;
        this.token = token;
        this.sliceX = sliceX;
    }
    
    @Override
    public String toString() {
        return "JigsawCaptchaCheckForm{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                ", sliceX=" + sliceX +
                '}';
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getToken() {
        return token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }
    
    public Integer getSliceX() {
        return sliceX;
    }
    
    public void setSliceX(Integer sliceX) {
        this.sliceX = sliceX;
    }
}
