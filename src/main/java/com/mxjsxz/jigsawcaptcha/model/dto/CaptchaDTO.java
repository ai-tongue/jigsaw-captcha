package com.mxjsxz.jigsawcaptcha.model.dto;

/**
 * 拼图验证码
 *
 * @author xdl
 * @since 2019-9-5
 */
public class CaptchaDTO {
    
    /**
     * 滑动拼图块
     */
    private String sliceImg;
    
    /**
     * 背景图
     */
    private String bgImg;
    
    /**
     * 注册token
     */
    private String token;
    
    /**
     * 拼图所在x坐标
     */
    private Integer x;
    
    /**
     * 拼图所在y坐标
     */
    private Integer y;
    
    public CaptchaDTO() {
    }
    
    public CaptchaDTO(String sliceImg, String bgImg, String token, Integer x, Integer y) {
        this.sliceImg = sliceImg;
        this.bgImg = bgImg;
        this.token = token;
        this.x = x;
        this.y = y;
    }
    
    @Override
    public String toString() {
        return "JigsawCaptchaDTO{" +
                "sliceImg='" + sliceImg + '\'' +
                ", bgImg='" + bgImg + '\'' +
                ", token='" + token + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
    
    public String getSliceImg() {
        return sliceImg;
    }
    
    public void setSliceImg(String sliceImg) {
        this.sliceImg = sliceImg;
    }
    
    public String getBgImg() {
        return bgImg;
    }
    
    public void setBgImg(String bgImg) {
        this.bgImg = bgImg;
    }
    
    public String getToken() {
        return token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }
    
    public Integer getX() {
        return x;
    }
    
    public void setX(Integer x) {
        this.x = x;
    }
    
    public Integer getY() {
        return y;
    }
    
    public void setY(Integer y) {
        this.y = y;
    }
}
