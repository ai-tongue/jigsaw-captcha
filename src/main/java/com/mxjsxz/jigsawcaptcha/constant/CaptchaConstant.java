package com.mxjsxz.jigsawcaptcha.constant;

/**
 * 拼图验证码 常量
 *
 * @author xdl
 * @since 2019-9-5
 */
public class CaptchaConstant {

    /**
     * 注册token名称
     */
    public static final String TOKEN = "token";

    /**
     * 拼图所在x坐标名称
     */
    public static final String X = "x";

    /**
     * 拼图允许误差
     */
    public static final Integer SLICE_DIFF_LIMIT = 5;

}
